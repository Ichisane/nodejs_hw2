const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('./models/Users');

require('dotenv').config();

const registerUser = async (req, res, next) => {
  const { username, password } = req.body;

  if (!username) {
    return res.status(400).json({ message: "Please specify 'username' parameter" });
  }

  if (!password) {
    return res.status(400).json({ message: "Please specify 'password' parameter" });
  }

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
    createdDate: new Date(),
  });

  return user.save()
    .then(() => res.json({ message: 'Success' }))
    .catch((err) => {
      next(err);
    });
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (!user) {
    return res.status(400).json({ message: 'User does not exist' });
  }
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { userId: user._id, username: user.username };
    const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
    return res.json({ message: 'Success', jwt_token: jwtToken });
  }
  return res.status(403).json({ message: 'Wrong password' });
};

module.exports = {
  registerUser,
  loginUser,
};
