const bcrypt = require('bcryptjs');
const User = require('./models/Users');

const getUserInfo = async (req, res) => {
  const userInfo = await User.findOne({ userId: req.user.userId }, '-__v');
  if (!userInfo) {
    return res.status(400).json({ message: 'User does not exist' });
  }
  const { _id, username, createdDate } = userInfo;
  return res.json({ user: { _id, username, createdDate } });
};

const deleteUser = async (req, res) => {
  const deletedUser = await User.findByIdAndDelete(req.user.userId);
  if (!deletedUser) {
    return res.status(400).json({ message: 'User does not exist' });
  }
  return res.json({ message: 'Success' });
};

const updateUser = async (req, res) => {
  const updatedUser = await User.findById(req.user.userId);
  if (!updatedUser) {
    return res.status(400).json({ message: 'User does not exist' });
  }
  const { oldPassword, newPassword } = req.body;
  if (!oldPassword || !newPassword) {
    return res.status(400).json({ message: 'Wrong structure of changing password' });
  }
  if (await bcrypt.compare(String(oldPassword), String(updatedUser.password))) {
    updatedUser.password = await bcrypt.hash(newPassword, 10);
    return updatedUser.save().then(() => res.json({ message: 'Success' }));
  }
  return res.status(400).json({ message: 'Wrong old password' });
};

module.exports = {
  getUserInfo,
  deleteUser,
  updateUser,
};
