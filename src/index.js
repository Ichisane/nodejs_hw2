const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

require('dotenv').config();

mongoose.connect(process.env.DB_CONN);

const { authRouter } = require('./authRouter');
const { usersRouter } = require('./usersRouter');
const { notesRouter } = require('./notesRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    app.listen(process.env.PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
