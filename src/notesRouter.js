const express = require('express');

const router = express.Router();
const {
  getUserNotes, addUserNote, getUserNote, updateUserNote, checkOrUncheckUserNote, deleteUserNote,
} = require('./notesService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/', authMiddleware, getUserNotes);
router.post('/', authMiddleware, addUserNote);
router.get('/:id', authMiddleware, getUserNote);
router.put('/:id', authMiddleware, updateUserNote);
router.patch('/:id', authMiddleware, checkOrUncheckUserNote);
router.delete('/:id', authMiddleware, deleteUserNote);

module.exports = {
  notesRouter: router,
};
